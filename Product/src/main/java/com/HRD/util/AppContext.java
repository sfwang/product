/**
 * Copyright (C) 2013 ChinaHrd.net - Product
 *
 *
 * @className:com.HRD.util.AppContext
 * @description:TODO
 * 
 * @version:v1.0.0
 * @author:sfwang@chinahrd.net
 * 
 * Modification History:
 * Date               Author                    Version     Description
 * -----------------------------------------------------------------
 * 2013��10��11��            sfwang@chinahrd.net       v1.0.0        create
 *
 *
 */
package com.HRD.util;
import java.util.ArrayList;
import java.util.List;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AppContext {
    private static AppContext instance;

    private volatile AbstractApplicationContext appContext;

    public synchronized static AppContext getInstance() {
        if (instance == null) {
            instance = new AppContext();
        }

        return instance;
    }

    private AppContext() {
        List<String> list = new ArrayList<String>();
        list.add("*.xml");

        String ss[] = list.toArray(new String[] {});
        for (int i = 0; i < ss.length; i++) {
            System.out.println("ss[" + i + "]" + ss[i]);

        }

        this.appContext = new ClassPathXmlApplicationContext(ss);
    }

    public AbstractApplicationContext getAppContext() {
        return appContext;
    }
}
