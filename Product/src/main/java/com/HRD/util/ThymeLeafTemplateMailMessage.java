/**
 * Copyright (C) 2013 ChinaHrd.net - Product
 *
 *
 * @className:com.HRD.util.SendEmail
 * @description:TODO
 * 
 * @version:v1.0.0
 * @author:sfwang@chinahrd.net
 * 
 * Modification History:
 * Date               Author                    Version     Description
 * -----------------------------------------------------------------
 * 2013��10��12��            sfwang@chinahrd.net       v1.0.0        create
 *
 *
 */
package com.HRD.util;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

/**
 * �ʼ�������
 * @���� chenlb
 * @����ʱ�� 2007-7-28 ����03:35:31 
 */
public class ThymeLeafTemplateMailMessage {

    protected final Log logger = LogFactory.getLog(getClass());
    private JavaMailSender javaMailSender;


    public void setTemplateEngine(TemplateEngine templateEngine) {
        this.templateEngine = templateEngine;
    }


    public JavaMailSender getJavaMailSender() {
        return javaMailSender;
    }

    // private VelocityEngine velocityEngine;
    private String from;
    private String title;
    private String[] toEmails;
    private TemplateEngine templateEngine;
    private Context model;


    public boolean send() {
        MimeMessage msg = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(msg,"utf-8");
        
        try {
            helper.setFrom(from);
            helper.setSubject(title);
            helper.setTo(toEmails);
            
           // System.out.println(getMessage());
            String str = getMessage();
            
            String str1=str.replaceAll("<!\\[CDATA\\[", "");
            String message=str1.replaceAll("]]>", "");
            System.out.println(message);
            System.out.println(message.indexOf("<![CDATA["));
            helper.setText(message, true);   //���Ĳ���html����ȥ��true����
            javaMailSender.send(msg);
            
        } catch (MessagingException e) {
            // TODO �Զ���� catch ��
            if(logger.isWarnEnabled()) {
                logger.warn("�ʼ���Ϣ����! �ʼ�����Ϊ: "+title);
            }
            return false;
            //e.printStackTrace();
        } catch (MailException me) {
            // TODO: handle exception
            if(logger.isWarnEnabled()) {
                logger.warn("�����ʼ�ʧ��! �ʼ�����Ϊ: "+title);
            }
            return false;
        }
        return true;    
    }
    
    
    /**
     * �ʼ�ģ���еõ���Ϣ
     * @return �����ط��͵�����
     */
    private String getMessage() {
        return templateEngine.process("simple-email.html", model);
    }

    private String[] createToEmail(String to) {
        return new String[] {to};
    }
    
    public void setToEmail(String to) {
        setToEmails(createToEmail(to));
    }
    
    public void setJavaMailSender(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }
    
   /* public void setVelocityEngine(VelocityEngine velocityEngine) {
        this.velocityEngine = velocityEngine;
    }*/

    public void setModel(Context model) {
        this.model = model;
    }


    public void setTitle(String title) {
        this.title = title;
    }

    public void setToEmails(String[] toEmails) {
        this.toEmails = toEmails;
    }

    public void setFrom(String from) {
        this.from = from;
    }

}
