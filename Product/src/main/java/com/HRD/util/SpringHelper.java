/**
 * Copyright (C) 2013 ChinaHrd.net - Product
 *
 *
 * @className:com.HRD.util.SpringHelper
 * @description:TODO
 * 
 * @version:v1.0.0
 * @author:sfwang@chinahrd.net
 * 
 * Modification History:
 * Date               Author                    Version     Description
 * -----------------------------------------------------------------
 * 2013��10��11��            sfwang@chinahrd.net       v1.0.0        create
 *
 *
 */
package com.HRD.util;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class SpringHelper implements ApplicationContextAware{

   static ApplicationContext  ctx=null;
    /**
     * ��ȡspring����ע��Ķ���
     * 
     * @param name
     * @return Object Bean
     */
    public static Object getBean(String name) {

        return ctx.getBean(name);
    }

    @Override
    public void setApplicationContext(ApplicationContext arg0)
            throws BeansException {
        SpringHelper.ctx = arg0;
        
    }
}
