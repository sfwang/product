/**
 * Copyright (C) 2013 ChinaHrd.net - Product
 *
 *
 * @className:com.HRD.util.Configuration
 * @description:TODO
 * 
 * @version:v1.0.0
 * @author:sfwang@chinahrd.net
 * 
 * Modification History:
 * Date               Author                    Version     Description
 * -----------------------------------------------------------------
 * 2013��10��11��            sfwang@chinahrd.net       v1.0.0        create
 *
 *
 */
package com.HRD.util;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


/**
 * ��ȡproperties�ļ�
 * @author 
 *
 */
public class Configuration
{
    private static Properties propertie;
    private InputStream in;
    private static Configuration config = new Configuration();
    
    /**
     * ��ʼ��Configuration��
     */
    public Configuration()
    {
        propertie = new Properties();
        try {
//            System.out.println(System.getProperty("user.dir"));
//            inputFile = new FileInputStream("cfg/config.properties");
            in =  ClassLoader.getSystemResourceAsStream("cfg/config.properties");
            propertie.load(in);
            in.close();
        } catch (FileNotFoundException ex) {
            System.out.println("��ȡ�����ļ�--->ʧ�ܣ�- ԭ���ļ�·����������ļ�������");
            ex.printStackTrace();
        } catch (IOException ex) {
            System.out.println("װ���ļ�--->ʧ��!");
            ex.printStackTrace();
        }        
    }
    

    
    /**
     * ���غ���õ�key��ֵ
     * @param key ȡ����ֵ�ļ�
     * @return key��ֵ
     */
    public static  String getValue(String key)
    {
        if(propertie.containsKey(key)){
            String value = propertie.getProperty(key);//�õ�ĳһ���Ե�ֵ
            return value;
        }
        else 
            return "";
    }//end getValue()

    


    
    public static void main(String[] args)
    {

        System.out.println(Configuration.getValue("mail.username"));
        System.out.println(System.getProperty("user.dir"));


        
    }//end main()



	public static Configuration getConfig() {
		return config;
	}



	public static void setConfig(Configuration config) {
		Configuration.config = config;
	}
    
}//end class ReadConfigInfo
