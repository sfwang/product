package com.HRD.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="product")
public class Product {
	private Integer isbn;
	private String name;
	private String d;
	public Product(String name, String d) {
		super();
		this.name = name;
		this.d = d;
	}
	public Product() {
		super();
	}
	@Id
	@GeneratedValue
	@Column
	public Integer getIsbn() {
		return isbn;
	}
	public void setIsbn(Integer isbn) {
		this.isbn = isbn;
	}
	@Column
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Column
	public String getD() {
		return d;
	}
	public void setD(String d) {
		this.d = d;
	}
	
}
