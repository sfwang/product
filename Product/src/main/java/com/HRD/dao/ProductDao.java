package com.HRD.dao;

import java.util.List;

import com.HRD.entity.Product;

public interface ProductDao {
	public List<Product> findall();
	public boolean save(Product product);
	public boolean update(Product product);
	public boolean delete(Product product);
	public Product findByIsbn(Product product);
}
