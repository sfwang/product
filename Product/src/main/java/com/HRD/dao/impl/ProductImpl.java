package com.HRD.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Component;

import com.HRD.dao.ProductDao;
import com.HRD.entity.Product;
@Component
public class ProductImpl implements ProductDao {
	
	private HibernateTemplate hibernateTemplate;
	
	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}
	@Autowired
	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	@SuppressWarnings("unchecked")
	public List<Product> findall() {
		// TODO Auto-generated method stub
		return hibernateTemplate.find("from Product");
	}

	public boolean save(Product product) {
		// TODO Auto-generated method stub
		try {
			hibernateTemplate.save(product);
			return true;
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		return false;
	}

	public boolean update(Product product) {
		// TODO Auto-generated method stub
		try {
			hibernateTemplate.update(product);
			return true;
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		return false;
	}

	public boolean delete(Product product) {
		// TODO Auto-generated method stub
		try {
			hibernateTemplate.delete(product);
			return true;
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		return false;
	}

	public Product findByIsbn(Product product) {
		// TODO Auto-generated method stub
		return hibernateTemplate.get(product.getClass(), product.getIsbn());
	}

}
