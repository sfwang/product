package com.HRD.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.HRD.Biz.ProductBiz;
import com.HRD.entity.Product;
@Controller
@RequestMapping(value="/product")
public class ProductController {
	private ProductBiz productBiz;
	
	public ProductBiz getProductBiz() {
		return productBiz;
	}
	@Autowired
	public void setProductBiz(ProductBiz productBiz) {
		this.productBiz = productBiz;
	}
	@RequestMapping(value="/show")
	public String add(@ModelAttribute(value="u1")Product product,Model model){
		List<Product> list=productBiz.findall();
		model.addAttribute("lable", "ss");
		model.addAttribute("list", list);
		return "show";
	}
	@RequestMapping(value="/add")
	public String add(@ModelAttribute(value="u1")Product product){
		boolean b=productBiz.dosave(product);
		if(b)
		{
			return "show";
		}
		return "err";
	}
	public void test(){
	    if(1==1){
	        System.out.println(11);
	    }
	}
}
