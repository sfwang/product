package com.HRD.Biz;

import java.util.List;

import com.HRD.entity.Product;

public interface ProductBiz {
	public List<Product> findall();
	public boolean dosave(Product product);
	public boolean doupdate(Product product);
	public boolean dodelete(Product product);
	public Product findByIsbn(Product product);
}	
