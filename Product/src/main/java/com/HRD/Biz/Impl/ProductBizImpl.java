package com.HRD.Biz.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.HRD.Biz.ProductBiz;
import com.HRD.dao.ProductDao;
import com.HRD.entity.Product;
@Component
public class ProductBizImpl implements ProductBiz {
	private ProductDao productDao;
	public final List<Product> findall() {
	    
		return productDao.findall();
	}

	public boolean dosave(Product product) {
		// TODO Auto-generated method stub
		return productDao.save(product);
	}

	public boolean doupdate(Product product) {
		// TODO Auto-generated method stub
		return productDao.update(product);
	}

	public  boolean dodelete( Product product) {
		// TODO Auto-generated method stub
		return productDao.delete(product);
	}

	public Product findByIsbn(Product product) {
		// TODO Auto-generated method stub
		return productDao.findByIsbn(product);
	}

	public final ProductDao getProductDao() {
		return productDao;
	}
	@Autowired
    public final void setProductDao( ProductDao productDao) {
		this.productDao = productDao;
	}
}
