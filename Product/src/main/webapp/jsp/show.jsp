<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Insert title here</title>
</head>
<body>
<c:if test="${empty lable }">
	<c:redirect url="/product/show.shtml"></c:redirect>
</c:if>
	<h3>It's a test page.</h3>
	<a href="/Product/jsp/add.jsp">添加商品</a>
	<div id="display">
		<table>
			<tr><td>序号</td><td>昵称</td><td>介绍</td><td>操作</td></tr>
			<c:forEach var="product" items="${list}">
			<tr>
				<td>${product.isbn}</td>
				<td>${product.name}</td>
				<td>${product.d}</td>
				<td><a href="delete.shtml?id=${product.isbn}">删除</a></td>
			</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>