package com.HRD.test;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.thymeleaf.context.Context;

import com.HRD.util.ThymeLeafTemplateMailMessage;

@RunWith(SpringJUnit4ClassRunner.class) 
@ContextConfiguration(locations = "classpath:applicationContext.xml") 
public class EmailTest {
    @Autowired
    private ThymeLeafTemplateMailMessage templateMail;
    @Autowired
    private JavaMailSender mailSender;

    @Test
    public void test() {        
        try {
            Context data = new Context();
            data.setVariable("username", "小明");
            data.setVariable("id", "12544343243243");
            data.setVariable("token", "143qwrfdsa32fdsaf");
            templateMail.setJavaMailSender(mailSender);
            templateMail.setModel(data);
            templateMail.setToEmail("2196913782@qq.com");
            templateMail.setTitle("邮件主题");
            templateMail.send();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}

